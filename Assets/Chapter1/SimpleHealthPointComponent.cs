using System.Collections;
using System.Collections.Generic;
using Unity.VisualScripting;
using UnityEngine;

namespace Theerapat.GameDev3.Chapter1
{
    public class SimpleHealthPointComponent : MonoBehaviour
    {
        [SerializeField] public const float MAX_HP = 100;

        [SerializeField] private float m_HealthPoint;

        public float HealthPoint
        {
            get
            {
                return m_HealthPoint;
            }
            set
            {
                if (value > 0)
                {
                    m_HealthPoint = value;
                   // if (value <= MAX_HP)
                    //{
                    //    m_HealthPoint = value;
                    //}
                   // else
                    //{
                    //    m_HealthPoint = MAX_HP;
                    //}
                }
            }
        }
        // Start is called before the first frame update
        void Start()
        {
        
        }

        // Update is called once per frame
        void Update()
        {
        
        }
    }
}

