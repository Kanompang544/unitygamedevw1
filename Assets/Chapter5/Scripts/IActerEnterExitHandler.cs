using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Theerapat.Gamedev3.Chapter5.InteractionSystem
{
    public interface IActorEnterExitHandler
     {
         void ActorEnter(GameObject actor);
         void ActorExit(GameObject actor);
     }
}
