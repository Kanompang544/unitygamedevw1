using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Theerapat.Gamedev3.Chapter5.InteractionSystem
{
    public interface IInteractable
     {
         void Interact(GameObject actor);
     }
}
