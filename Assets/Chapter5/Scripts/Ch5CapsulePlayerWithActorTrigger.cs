using System.Collections;
using System.Collections.Generic;
using Theerapat.Gamedev3.Chapter5.InteractionSystem;
using UnityEngine;
using UnityEngine.InputSystem;

namespace  Theerapat.Gamedev3.Chapter5.PlayerController
{
    public class Ch5CapsulePlayerWithActorTrigger :
        Ch5CapsulePlayerControllerWithPreset
     {
         [SerializeField] protected ActorTriggerHandler m_ActorTriggerHandler;
    
     protected override void Update()
     {
         base.Update();
        
         Keyboard keyboard = Keyboard.current;
        
         if (keyboard[m_Preset.InteractionKey].wasPressedThisFrame)
             {
             PerformInteraction();
             }
         }
    
     protected virtual void PerformInteraction()
     {
         var interactable = m_ActorTriggerHandler.GetInteractable();
        
         if (interactable != null)
             {
             interactable.Interact(gameObject);
             }
         }
     }
}
