using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Theerapat.Gamedev3.Chapter5.PlayerController
{
    public interface IPlaterController
        {
            void MoveForward();
            void MoveForwardSprint();

            void MoveBackward();

            void TurnLeft();
            void TurnRight();
        }
        // Start is called before the first frame update
        
    }
  

