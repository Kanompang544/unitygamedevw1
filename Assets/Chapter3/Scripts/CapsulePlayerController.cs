using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.InputSystem;

namespace Theerapat.Gamedev3.Chapter3
{
    public class CapsulePlayerController : MonoBehaviour
    {
        [SerializeField] public float m_RotationSpeed = 180;

        [SerializeField] public float m_DirectionalSpeed = 3;

        [SerializeField] protected float m_DirectionalSprintSpeed = 5;

        [Header("Keys Config")] [SerializeField]
        protected Key m_ForwardKey = Key.W;

        [SerializeField] protected Key m_BackwardKey = Key.S;

        [SerializeField] public Key m_TurnLeftKey = Key.A;

        [SerializeField] public Key m_TurnRightKey = Key.D;
        // Start is called before the first frame update
        void Start()
        {
        
        }

        // Update is called once per frame
        void Update()
        {
            Keyboard keyboard = Keyboard.current;

            if (keyboard[m_TurnLeftKey].IsPressed())
            {
                transform.Rotate(transform.up,-m_RotationSpeed*Time.deltaTime,Space.Self);
            }
            else if (keyboard[m_TurnRightKey].IsPressed())
            {
                transform.Rotate(transform.up,m_RotationSpeed*Time.deltaTime,Space.Self);
            }

            float speedMagnitude = m_DirectionalSpeed;
            if (keyboard[Key.LeftShift].IsPressed())
            {
                speedMagnitude = m_DirectionalSpeed;
            }

            if (keyboard[m_ForwardKey].IsPressed())
            {
                transform.Translate(transform.forward*speedMagnitude*Time.deltaTime,Space.World);
            }
            else if (keyboard[m_BackwardKey].IsPressed())
            {
                transform.Translate(-transform.forward*(speedMagnitude*0.4f)*Time.deltaTime,Space.World);
            }
        }
    }
}

