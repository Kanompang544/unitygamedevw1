using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Theerapat.Gamedev3.Chapter9.Interaction
{
    [CreateAssetMenu(menuName = "GameDev3/Util/ScriptableObjectUtils")]
    public class ScriptableObjectUtils : ScriptableObject
    {
        public void Destroy(GameObject gameObject)
        {
            Object.Destroy(gameObject);
            }
        }
}