using System.Collections;
using System.Collections.Generic;
using Theerapat.Gamedev3.Chapter5.InteractionSystem;
using UnityEngine;
using UnityEngine.Events;

namespace Theerapat.Gamedev3.Chapter6.UnityEvents
{
    public class GenericInteracable : MonoBehaviour , IInteractable ,
        IActorEnterExitHandler
     {
         [SerializeField] protected UnityEvent<GameObject> m_OnInteract = new();
    
     [SerializeField] protected UnityEvent<GameObject> m_OnActorEnter = new();
     [SerializeField] protected UnityEvent<GameObject> m_OnActorExit = new();
    
     public virtual void Interact(GameObject actor)
     {
         m_OnInteract.Invoke(actor);
         }
    
     public virtual void ActorEnter(GameObject actor)
     {
         m_OnActorEnter.Invoke(actor);
         }
    
     public virtual void ActorExit(GameObject actor)
     {
         m_OnActorExit.Invoke(actor);
         }
     }
}