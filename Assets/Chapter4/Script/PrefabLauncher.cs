using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Theerapat.Gamedev3.Chapter4
{
    public class PrefabLauncher : MonoBehaviour
    {
        [SerializeField] protected float m_Interval = 1.2f;

        [SerializeField] protected GameObject m_PrefabTobeLaunched;

        [SerializeField] protected float m_Power = 10;
        // Start is called before the first frame update
        void Start()
        {
            Invoke(nameof(LaunchPrefab),0);
        }

        protected void LaunchPrefab()
        {
            var g = Instantiate(m_PrefabTobeLaunched);
            g.transform.position = transform.position;
            var rb = g.GetComponent<Rigidbody>();
            rb.AddForce(transform.forward*m_Power,ForceMode.Impulse);
            Destroy(g,3);
            Invoke(nameof(LaunchPrefab),m_Interval);
        }
        // Update is called once per frame
        void Update()
        {
        
        }
    }

}
