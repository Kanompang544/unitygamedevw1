using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Theerapat.GameDev3.Chapter1;
using Theerapat.Gamedev3.Chapter3;
using UnityEngine.InputSystem;

namespace Theerapat.Gamedev3.Chapter4
{
    public class PlayerTriggerWithITC : MonoBehaviour
    {
        private void OnTriggerEnter(Collider other)
        {
            ItemTypeComponent itc = other.GetComponent<ItemTypeComponent>();
            var inventory = GetComponent<Inventory>();
            var simpleHP = GetComponent<SimpleHealthPointComponent>();
            var Speed = GetComponent<CapsulePlayerController>();
            if (itc != null)
            {
                switch (itc.Type)
                {
                    case ItemType.COIN:
                        inventory.AddItem("COIN", 1);
                        break;
                    case ItemType.BIGCOIN:
                        inventory.AddItem("BIGCOIN", 1);
                        break;
                    case ItemType.POWERUP:
                        if (simpleHP != null)
                            simpleHP.HealthPoint = simpleHP.HealthPoint + 10;
                        break;
                    case ItemType.POWERDOWN:
                        if (simpleHP != null)
                            simpleHP.HealthPoint = simpleHP.HealthPoint - 10;
                        break;
                    case ItemType.SPEEDUP:
                        Speed.m_DirectionalSpeed = Speed.m_DirectionalSpeed + 10;
                        break;
                    case ItemType.DIZZY:
                        Speed.m_RotationSpeed = Speed.m_RotationSpeed + 1000;
                        Speed.m_TurnLeftKey = Key.D;
                        Speed.m_TurnRightKey = Key.A;
                        break;

                }
            }
            Destroy(other.gameObject, 0);
        }

        // Start is called before the first frame update
        void Start()
        {
        
        }

        // Update is called once per frame
        void Update()
        {
        
        }
    }

}
