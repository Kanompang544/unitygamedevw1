using System.Collections;
using System.Collections.Generic;
using System.Diagnostics;
using Theerapat.Gamedev3.Chapter4;
using UnityEngine;

namespace Theerapat.Gamedev3.Chapter4
{
    
}
public class ItemTriggerWithPlayerhandler : MonoBehaviour
{
    protected virtual void OnTriggerEnter(Collider other)
    {
        if (other.CompareTag("Player"))
        {
            if (gameObject.CompareTag("Diamond"))
            {
                ProcessTriggerWithDiamond();
            }
            else if (gameObject.CompareTag("Coin"))
            {
                ProcessTriggerWithCoin();
            }

            var inventory = other.GetComponent<Inventory>();
            inventory.AddItem(gameObject.tag,1);
            Destroy(gameObject);
        }
    }

    protected virtual void ProcessTriggerWithDiamond()
    {
        
    }

    protected virtual void ProcessTriggerWithCoin()
    {
        
    }
    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        
    }
}
