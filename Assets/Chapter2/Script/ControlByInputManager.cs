using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Theerapat.Gamedev3.Chapter2
{
    public class ControlByInputManager : MonoBehaviour
    {
        public float m_MovementScale = 0.5f;
        // Start is called before the first frame update
        void Start()
        {
        
        }

        // Update is called once per frame
        void Update()
        {
            float hMovement = Input.GetAxis("Horizontal") * m_MovementScale;
            float vMovement = Input.GetAxis("Vertical") * m_MovementScale;
            
            this.transform.Translate(new Vector3(hMovement, vMovement, 0));

            if (Input.GetButtonDown("Fire1"))
            {
                ShootABulletInForwardDirection(Vector3.zero);
            }

            if (Input.GetButtonDown("Fire2"))
            {
                ShootABulletInForwardDirection(new Vector3(0, 1, 0), PrimitiveType.Capsule, 1.5f);
            }
        }

        private void ShootABulletInForwardDirection(Vector3 forceModifier, PrimitiveType type = PrimitiveType.Sphere,
            float forcemagnitude = 1)
        {
            var newGameObject = GameObject.CreatePrimitive(type);
            newGameObject.transform.position = transform.position + transform.forward * 1.5f;

            var rigidbody = newGameObject.AddComponent<Rigidbody>();
            rigidbody.mass = 0.15f;

            Vector3 shootingDirection = (forceModifier + transform.forward) * forcemagnitude;
            rigidbody.AddForce(shootingDirection, ForceMode.Impulse);
            
            Destroy(newGameObject, 3);
        }
    }
}