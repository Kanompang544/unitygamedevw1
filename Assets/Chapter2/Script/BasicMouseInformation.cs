using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

namespace Theerapat.Gamedev3.Chapter2
{
    public class BasicMouseInformation : MonoBehaviour
    {
        public Text m_TextMousePosition;

        public Text m_TextMouseScrollDelta;

        public Text m_TextMouseDeltaVector;

        private Vector3 m_MousePreviousPosition;
        // Start is called before the first frame update
        void Start()
        {
        
        }

        // Update is called once per frame
        void Update()
        {
            Vector3 mouseCurrentPos = Input.mousePosition;
            Vector3 mouseDeltavector = Vector3.zero;
            mouseDeltavector = (mouseCurrentPos - m_MousePreviousPosition).normalized;
            
            m_TextMousePosition.text = Input.mousePosition.ToString();
            m_TextMouseScrollDelta.text = Input.mouseScrollDelta.ToString();
            m_TextMouseDeltaVector.text = mouseDeltavector.ToString();

            m_MousePreviousPosition = mouseCurrentPos;
        }
    }
}