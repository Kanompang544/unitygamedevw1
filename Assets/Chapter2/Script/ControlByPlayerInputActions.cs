using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.InputSystem;
using Theerapat.GameDev3.Chapter1;

namespace Theerapat.Gamedev3.Chapter2
{
    public class ControlByPlayerInputActions : MonoBehaviour
    {
        public float m_MovementScale = 0.5f;

        protected float m_CurrentHorizontal;

        protected float m_CurrentVertical;

        protected bool m_IsFire1Held = false;

        protected bool m_IsFire2Held = false;

        public void OnHorizontal(InputValue value)
        {
            m_CurrentHorizontal = value.Get<float>();
        }

        public void OnVertical(InputValue value)
        {
            m_CurrentVertical = value.Get<float>();
        }

        public void OnFire1(InputValue value)
        {
            Debug.Log("Fire1");
            var v = value.Get<float>();

            if (value.isPressed)
            {
                ShootABulletInForwardDirection(Vector3.zero);

                if (v > UnityEngine.InputSystem.InputSystem.settings.defaultHoldTime)
                {
                    m_IsFire1Held = true;
                }
            }
            else
                {
                    ShootABulletInForwardDirection(new Vector3(0, 1, 0), PrimitiveType.Capsule, 1.5f);

                    m_IsFire1Held = false;
                }
            }
            public void OnFire2(InputValue value){
                Debug.Log("Fire2");
                var v = value.Get<float>();

                if (value.isPressed)
                {
                    var spinMovement = gameObject.AddComponent<SpinMovement>();

                    if (v > UnityEngine.InputSystem.InputSystem.settings.defaultHoldTime)
                    {
                        m_IsFire2Held = true;
                    }
                }
                else
                {
                    var spinMovement = gameObject.GetComponent<SpinMovement>();
                    Destroy(spinMovement);

                    m_IsFire2Held = false;
                }
            }
            
        // Start is called before the first frame update
        void Start()
        {
        
        }

        // Update is called once per frame
        void Update()
        {
            float hMovement = m_CurrentHorizontal * m_MovementScale;
            float vMovement = m_CurrentVertical * m_MovementScale;
            
            transform.Translate(new Vector3(hMovement, vMovement,0));

            if (m_IsFire1Held)
            {
                Debug.Log("Fire1-Hold");
            }

            if (m_IsFire2Held)
            {
                Debug.Log("Fire2-Hold");
            }
        }

        private void ShootABulletInForwardDirection(Vector3 forceModifier, PrimitiveType type = PrimitiveType.Sphere,
            float forceMagnitude = 1)
        {
            var newGameObject = GameObject.CreatePrimitive(type);
            newGameObject.transform.position = transform.position + transform.forward * 1.5f;
            var rigidbody = newGameObject.AddComponent<Rigidbody>();
            rigidbody.mass = 0.15f;
            Vector3 shootingDirection = (forceModifier + transform.forward) * forceMagnitude;
            rigidbody.AddForce(shootingDirection, ForceMode.Impulse);
            Destroy(newGameObject,3);
        }
    }
}