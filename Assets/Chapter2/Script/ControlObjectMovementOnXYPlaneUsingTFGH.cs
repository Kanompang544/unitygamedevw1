using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Theerapat.Gamedev3.Chapter2
{
    public class ControlObjectMovementOnXYPlaneUsingTFGH : MonoBehaviour
    {
        public float m_MovementStep;
        // Start is called before the first frame update
        void Start()
        {
        
        }

        // Update is called once per frame
        void Update()
        {
            if (Input.GetKey(KeyCode.F))
            {
                //this.transform.Translate(-m_MovementStep, 0, 0);
            }
            else if (Input.GetKey(KeyCode.H))
            {
                //this.transform.Translate(m_MovementStep, 0, 0);
            }
            else if (Input.GetKey(KeyCode.T))
            {
                //this.transform.Translate(0, m_MovementStep, 0);
            }
            else if (Input.GetKey(KeyCode.G))
            {
                //this.transform.Translate(0, -m_MovementStep, 0);
            }

            if (Input.GetKeyUp(KeyCode.F))
            {
                this.transform.Translate(-m_MovementStep*(-1), 0, 0);
            }
            else if (Input.GetKeyUp(KeyCode.H))
            {
                this.transform.Translate(m_MovementStep*(-1), 0, 0);
            }
            else if (Input.GetKeyUp(KeyCode.T))
            {
                this.transform.Translate(0, m_MovementStep*(-1), 0);
            }
            else if (Input.GetKeyUp(KeyCode.G))
            {
                this.transform.Translate(0, -m_MovementStep*(-1), 0);
            }
        }
    }
}