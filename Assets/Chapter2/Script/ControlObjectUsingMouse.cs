using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Assertions.Must;

namespace Theerapat.Gamedev3.Chapter2
{
    public class ControlObjectUsingMouse : MonoBehaviour
    {
        private Vector3 m_MousePreviousPosition; 
        public float m_MouseDeltaVectorScaling = 0.5f;
        // Start is called before the first frame update
        void Start()
        {
        
        }

        // Update is called once per frame
        void Update()
        {
            Vector3 mouseCurrentPos = Input.mousePosition;
            Vector3 mouseDeltaVector = Vector3.zero;
            mouseDeltaVector = (mouseCurrentPos - m_MousePreviousPosition).normalized;;

            if (Input.GetMouseButton(0))
            {
                this.transform.Translate(mouseDeltaVector * m_MouseDeltaVectorScaling, Space.World);
            }
            this.transform.Translate(0, 0, Input.mouseScrollDelta.y * m_MouseDeltaVectorScaling, Space.World);
            m_MousePreviousPosition = mouseCurrentPos;
            if (Input.GetMouseButtonUp(1))
            {
                Debug.Log("Fire2-Up");
                this.transform.Translate(0,0,mouseDeltaVector.z-=m_MouseDeltaVectorScaling);
            }

            if (Input.GetMouseButtonDown(2))
            {
                Debug.Log("MiddleMouse-Down");
                this.transform.Translate(0,0,mouseDeltaVector.z += m_MouseDeltaVectorScaling);
            }
        }
    }
}