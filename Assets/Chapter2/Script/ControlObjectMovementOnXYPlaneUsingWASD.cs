using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Theerapat.Gamedev3.Chapter2
{
    public class ControlObjectMovementOnXYPlaneUsingWASD : MonoBehaviour
    {
        public float m_MovementStep;
        // Start is called before the first frame update
        void Start()
        {
        
        }

        // Update is called once per frame
        void Update()
        {
            if (Input.GetKey(KeyCode.A))
            {
                this.transform.Translate(-m_MovementStep, 0, 0);
            }
            else if (Input.GetKey(KeyCode.D))
            {
                this.transform.Translate(m_MovementStep, 0, 0);
            }
            else if (Input.GetKey(KeyCode.W))
            {
                this.transform.Translate(0, m_MovementStep, 0);
            }
            else if (Input.GetKey(KeyCode.S))
            {
                this.transform.Translate(0, -m_MovementStep, 0);
            }
        }
    }
}